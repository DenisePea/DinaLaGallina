<?php   
  // Prevent from direct access
  if (! defined('ROOT_URL')) {
    die;
  }
?>

<!-- Contenuto della pagina homepage: -->
<h1>Benvenuti</h1>
 <p class="lead">Benvenuti nel sito!</p>
 <p class="lead">Clicca qui per iniziare gli acquisti</p>
 <a href="<?php echo ROOT_URL . 'shop?page=product-list'; ?>" class="btn btn-primary btn-lg mb-5 mt-3">Vai allo Shopping &raquo;</a>