## Table of Contents
1. [Introduction](#introduction)
2. [General Design Constraints](#general-design-constraints)
3. [User Interface](#user-interface)
4. [System Features](#system-features)

## Introduction
***
DinaLaGallina � un'applicazione basata sul mondo avicolo. Nata per la compravendita o lo scambio di: pulcini, galline e galli.
� possibile creare una descrizione dell'animale e inserire i propri dati di contatto.
In questo documento si potr� capire come utilizzare il sito.

# Goals and Objectives
***
Gli obiettivi principali di DLG sono:
1. Fornire un modo semplice di interpretare l'applicazione
2. Rendere partecipi pi� persone 
3. Facilitare la compravendita o lo scambio dell'animale.
4. Fornire un metodo sicuro per assicurarsi della purezza dell'animale.

# Scope
***
L'innovativa applicazione permetter� di unire pi� persone in un unico posto, senza porre limiti geografici.

## General Design Constraints
***
Questo sito � stato scritto con il linguaggio HTML e php. 

# User Characteristics
***
DLG � utilizzabile da tutti, non bisogna avere esperienza nel campo avicolo.

## User Interface 
***
L'utente sar� in grado in meno di dieci secondi di capire il funzionamento del sito.

## System Features
***
Sul sito di DinaLaGallina sar� possibile vedere:
1. Una pagina di benvenuto
2. Avere un'area riservata
3. La homepage 
4. Il carrello 
