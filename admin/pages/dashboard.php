<?php
// Prevent from direct access (ROOT_URL nella cartella init)
if (! defined('ROOT_URL')) {
    die;
    }

global $loggedInUser;
?>
<!-- Le informazioni dell'utente' -->
<h1>Le informazioni di <?php echo $loggedInUser->first_name; ?></h1>

<div class="card mb-3 mt-3">
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><a href="<?php echo ROOT_URL; ?>admin?page=orders-list" class="underline">Gestione Ordini &raquo;</a></li> <!-- Prendo la pagina orders-list -->
    <li class="list-group-item"><a href="<?php echo ROOT_URL; ?>admin?page=products-list" class="underline">Gestione Prodotti &raquo;</a></li> <!-- Prendo la pagina products-list -->
    <li class="list-group-item"><a href="<?php echo ROOT_URL; ?>admin?page=users-list" class="underline">Gestione Utenti &raquo;</a></li> <!-- Prendo la pagina users-list -->
  </ul>
</div>