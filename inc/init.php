<?php 

    define('ROOT_URL', 'http://localhost/DinaLaGallina/');
    define('ROOT_PATH', 'C:\\xampp\\htdocs\DinalaGallina\\');
    define('SITE_NAME', 'DinaLaGallina');
    
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'dinalagallina');
    
    require_once ROOT_PATH . '/inc/include-classes.php';
    require_once ROOT_PATH . '/inc/globals.php';
    require_once ROOT_PATH . '/inc/functions.php';
    require_once ROOT_PATH . '/inc/authorize.php';
?>