<?php
// Prevent from direct access
if (! defined('ROOT_URL')) {
    die;
}

function esc($str) { 
    global $conn;
    return mysqli_real_escape_string($conn, htmlspecialchars($str)); //escape dei caratteri speciali in una stringa da utilizzare in una query SQL, tenendo conto del set di caratteri corrente della connessione.
}

function esc_html($str) {
    return htmlspecialchars($str); 
}

function shorten($str) { 
    return substr($str, 0, 30) . '...'; //ritorna una parte di una stringa
}

function random_string(){
    return substr(md5(mt_rand()), 0, 20); //calcola l'hash (metodo di sicurezza) md5 di una stringa
}
  
 ?>